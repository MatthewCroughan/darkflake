{
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-24.11";
  outputs = { nixpkgs, ... }:
  let
    pkgs = nixpkgs.legacyPackages.x86_64-linux;
  in
  {
    packages.${pkgs.hostPlatform.system}.cuphead = pkgs.callPackage ./default.nix {};
  };
}
