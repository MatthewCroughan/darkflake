{ lib
, fetchtorrent
, makeDesktopItem
, symlinkJoin
, writeShellScriptBin
, runCommand
, bottles
, cacert
, pname ? "cuphead"
, location ? "$HOME/.local/share/bottles/bottles/${pname}"
, tree
, innoextract
}:
let
  #TODO: Automate this fetching somehow, maybe flake inputs?
  components = {
    dxvk = builtins.fetchTarball {
      url = "https://github.com/bottlesdevs/components/releases/download/dxvk-2.2-2-bd575a4/dxvk-2.2-2-bd575a4.tar.gz";
      sha256 = "0wngvvdb684w4whx8jrh29vikiyis1nsxwhzw20yb00vfb5dgs8i";
    };
    vkd3d = builtins.fetchTarball {
      url = "https://github.com/bottlesdevs/components/releases/download/vkd3d-proton-2.9-1-87b0c4a/vkd3d-proton-2.9-1-87b0c4a.tar.gz";
      sha256 = "0bpmrvamrr0gnbrd1n4smns7g3kzj8zqiwgziqf9ha5f3d8vgygk";
    };
    nvapi = builtins.fetchTarball {
      url = "https://github.com/bottlesdevs/components/releases/download/dxvk-nvapi-v0.6.3-1-cb61c3f/dxvk-nvapi-v0.6.3-1-cb61c3f.tar.gz";
      sha256 = "1qrqda4d2rqlf6sr3b8fd9v0sgc3alq5cz8qhjxvhv3b6klay86m";
    };
    latencyflex = builtins.fetchTarball {
      url = "https://github.com/bottlesdevs/components/releases/download/latencyflex-v0.1.1-1-73bcb07/latencyflex-v0.1.1-1-73bcb07.tar.gz";
      sha256 = "0spgfqk330ahj3h9mb3r6r1ip6nbmqk6cwmyxhzymzlp719wdnha";
    };
    runners = {
      soda = builtins.fetchTarball {
        url = "https://github.com/bottlesdevs/wine/releases/download/soda-7.0-9/soda-7.0-9-x86_64.tar.xz";
        sha256 = "0hk2yykdd90zhb2zp4xvrn2fmqfv7ij2173hcz8znwfbx6qa0rba";
      };
    };
  };

  src = (fetchtorrent {
    config = { peer-limit-global = 1000; };
    url = "magnet:?xt=urn:btih:98222F24217C09AA2583DE54C13222711D8289EE&dn=Cuphead-GOG&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969%2Fannounce&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A6969%2Fannounce&tr=udp%3A%2F%2Ftracker.bittor.pw%3A1337%2Fannounce&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337&tr=udp%3A%2F%2Fbt.xxx-tracker.com%3A2710%2Fannounce&tr=udp%3A%2F%2Fpublic.popcorn-tracker.org%3A6969%2Fannounce&tr=udp%3A%2F%2Feddie4.nl%3A6969%2Fannounce&tr=udp%3A%2F%2Ftracker.torrent.eu.org%3A451%2Fannounce&tr=udp%3A%2F%2Fp4p.arenabg.com%3A1337%2Fannounce&tr=udp%3A%2F%2Ftracker.tiny-vps.com%3A6969%2Fannounce&tr=udp%3A%2F%2Fopen.stealth.si%3A80%2Fannounce";
    hash = "sha256-R1lquOWAcf8nA2Q2t7apo9lTKsosTFAMnJqUgjWYoo0=";
  }) + "/";

  gameData = runCommand "gameData" {
    buildInputs = [ innoextract ];
  } ''
    mkdir -p $out
    innoextract "${src}/setup_cuphead_20170929_(15295).exe"
    mv app/* $out
  '';

  gamingBottle = runCommand "blah" {
    buildInputs = [ bottles cacert tree ];
  } ''
      HOME=$TMP
      mkdir -p ~/.local/share/bottles/runners
      mkdir -p ~/.local/share/bottles/dxvk
      mkdir -p ~/.local/share/bottles/vkd3d
      mkdir -p ~/.local/share/bottles/nvapi
      mkdir -p ~/.local/share/bottles/latencyflex

      cp -r --no-preserve=owner ${components.dxvk} ~/.local/share/bottles/dxvk/dxvk-2.2
      cp -r --no-preserve=owner ${components.vkd3d} ~/.local/share/bottles/vkd3d/vkd3d-proton-2.9
      cp -r --no-preserve=owner ${components.nvapi} ~/.local/share/bottles/nvapi/dxvk-nvapi-v0.6.3
      cp -r --no-preserve=owner ${components.latencyflex} ~/.local/share/bottles/latencyflex/latencyflex-v0.1.1
      cp -r --no-preserve=owner ${components.runners.soda} ~/.local/share/bottles/runners/soda-7.0-9
      chmod -R u+rw,g+rw ~/.local
      echo "{}" > ~/.local/share/bottles/data.yml
      echo "{}" > ~/.local/share/bottles/library.yml

      tree -L 3 ~/.local/share/bottles

      bottles-cli new --bottle-name ${pname} --environment gaming
      bottles-cli standalone -b ${pname}

      cp -r ~/.local/share/bottles $out
  '';

  icon = null;

  script = writeShellScriptBin pname ''
    PATH=${bottles}/bin:$PATH
    if [ ! -d "${location}" ]; then
      # install gamingBottle for the first time
      #TODO: potentially destructive if user already uses bottles?
      cp -r --no-preserve=owner ${gamingBottle} ~/.local/share/bottles
      chmod -R u+rw,g+rw ~/.local/share/bottles/bottles/${pname}
      echo "disable" > ${location}/.update-timestamp
    fi
    bottles-cli run -e "${gameData}/Cuphead.exe" -b ${pname}
  '';

  desktopItems = makeDesktopItem {
    name = pname;
    exec = "${script}/bin/${pname}";
    inherit icon;
    comment = "Cuphead is a classic run and gun action game heavily focused on boss battles.";
    desktopName = "Cuphead";
    categories = [ "Game" ];
  };

in
symlinkJoin {
  name = pname;
  paths = [ desktopItems script ];

  meta = {
    description = "Cuphead is a classic run and gun action game heavily focused on boss battles.";
    homepage = "https://www.cupheadgame.com/";
    maintainer = lib.maintainers.matthewcroughan;
    platforms = [ "i686-linux" "x86_64-linux" ];
  };
}
